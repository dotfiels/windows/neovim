return {
	"neovim/nvim-lspconfig",
	dependencies = {
		{'hrsh7th/cmp-nvim-lsp'},
		{'hrsh7th/nvim-cmp'},
	},
	config = function()
		-- Reserve a space in the gutter
		-- This will avoid an annoying layout shift in the screen
		vim.opt.signcolumn = 'yes'

		-- Add cmp_nvim_lsp capabilities settings to lspconfig
		-- This should be executed before you configure any language server
		local lspconfig_defaults = require('lspconfig').util.default_config
		lspconfig_defaults.capabilities = vim.tbl_deep_extend(
		'force',
		lspconfig_defaults.capabilities,
		require('cmp_nvim_lsp').default_capabilities()
		)

		vim.api.nvim_create_autocmd('LspAttach', {
			desc = 'LSP actions',
			callback = function(event)
				local opts = {buffer = event.buf}

				vim.keymap.set('n', 'K', function () vim.lsp.buf.hover() end, opts)
				vim.keymap.set('n', 'gd', function () vim.lsp.buf.definition() end, opts)
				vim.keymap.set('n', 'gD', function () vim.lsp.buf.declaration() end, opts)
				vim.keymap.set('n', 'gi', function () vim.lsp.buf.implementation() end, opts)
				vim.keymap.set('n', 'go', function () vim.lsp.buf.type_definition() end, opts)
				vim.keymap.set('n', 'gr', function () vim.lsp.buf.references() end, opts)
				vim.keymap.set('n', 'gs', function () vim.lsp.buf.signature_help() end, opts)
				vim.keymap.set('n', '<F2>', function () vim.lsp.buf.rename() end, opts)
				vim.keymap.set({'n', 'x'}, '<F3>', function () vim.lsp.buf.format({async = true}) end, opts)
				vim.keymap.set('n', '<F4>', function () vim.lsp.buf.code_action() end, opts)
			end,
		})
	end
}
