return {
	"hrsh7th/nvim-cmp",
	dependencies = {
		'hrsh7th/cmp-nvim-lsp',
		'hrsh7th/cmp-buffer',
		'hrsh7th/cmp-path',
		'hrsh7th/cmp-cmdline',
		'neovim/nvim-lspconfig'
	},
	config= function()
		local cmp = require('cmp')

		cmp.setup({
			sources = {
				{name = 'nvim_lsp'},
				{ name = "neorg" },
			},
			snippet = {
				expand = function(args)
					-- You need Neovim v0.10 to use vim.snippet
					vim.snippet.expand(args.body)
				end,
			},
			mapping = cmp.mapping.preset.insert({
				['<C-Space>'] = cmp.mapping.complete(),
				['<S-NL>'] = cmp.mapping.scroll_docs(-4),
				['<C-S-K>'] = cmp.mapping.scroll_docs(-4),
				['	'] = cmp.mapping.confirm(),
				['<CR>'] = cmp.mapping.confirm({ select = true }),
			}),
		})
	end
}
