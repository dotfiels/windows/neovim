return {
	{
		"williamboman/mason.nvim",

		dependencies = {"williamboman/mason-lspconfig.nvim"},

		keys = {
			{ "<leader>m", ":Mason <CR>", desc = "open mason" },
		},
		config = true;
	},
	{
		"williamboman/mason-lspconfig.nvim",
		opts = {
			-- Replace the language servers listed here 
			-- with the ones you want to install
			ensure_installed = {'lua_ls', 'pyright'},
			handlers = {
				function(server_name)
					require('lspconfig')[server_name].setup({})
				end,
			},
		}
	},
}
