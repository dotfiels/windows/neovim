return {
	"nvim-neorg/neorg",
	lazy = true, -- Disable lazy loading as some `lazy.nvim` distributions set `lazy = true` by default
	dependencies = {
		{
			"juniorsundar/neorg-extras",
			-- tag = "v0.3.1" -- Always a safe bet to track current latest release
		},
		"nvim-telescope/telescope.nvim", -- Required for the Neorg-Roam features
		"nvim-lua/plenary.nvim", -- Required as part of Telescope installation
	},
	opts = {
		load = {
			["external.many-mans"] = {
				config = {
					metadata_fold = true, -- If want @data property ... @end to fold
					code_fold = true, -- If want @code ... @end to fold
				}
			},
			["core.defaults"] = {},
			["core.concealer"] = { config = { folds = false } },
			["core.dirman"] = {
				config = {
					workspaces = {
						school = "~/Documents/school",
						notes = "~/Documents/notes",
					},
					default_workspace = "notes",
				},
			},
			["core.completion"] = { config = { engine = "nvim-cmp" } },
		},
	},
	ft = "norg",
	cmd = "Neorg",
	config = true,
}
