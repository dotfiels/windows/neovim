vim.opt.tabstop = 4
vim.opt.softtabstop = 0
vim.opt.shiftwidth = 0
vim.opt.shell = "/bin/bash"
vim.cmd("set wrap smoothscroll")

vim.opt.rnu = true
vim.opt.nu = true

vim.cmd([[set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<,space:•]])
vim.keymap.set("n", "<leader>hc", ":set list!<CR>", { silent = true })

vim.opt.termguicolors = true

vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.scrolloff = 6

vim.cmd("nnoremap j gj")
vim.cmd("nnoremap k gk")
